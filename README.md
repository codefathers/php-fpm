# Codefathers Docker Image for PHP-FPM

This Docker Image provides a php-fpm runtime.
You can find a branch for supported PHP Versions, such as 5.6 or 7.1. 

By default it extends the public php-fpm docker image and comes with additional php modules. 

* gd
* mbstring
* pdo_mysql
* mysqli
* mcrypt
* iconv
* soap
* zip
* redis
* xdebug (disabled by default)

Its intended to use in a container envrionment e.q. with a nginx container.

For detailed or short documentations take a look at the readme.md of each branch. 
